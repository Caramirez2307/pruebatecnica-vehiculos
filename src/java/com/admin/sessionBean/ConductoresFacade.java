/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.sessionBean;

import com.admin.model.Conductores;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author USER
 */
@Stateless
public class ConductoresFacade extends AbstractFacade<Conductores> {

    @PersistenceContext(unitName = "vehiculoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConductoresFacade() {
        super(Conductores.class);
    }
    
}
