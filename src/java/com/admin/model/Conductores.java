/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "conductores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conductores.findAll", query = "SELECT c FROM Conductores c")
    , @NamedQuery(name = "Conductores.findByNumeroid", query = "SELECT c FROM Conductores c WHERE c.numeroid = :numeroid")
    , @NamedQuery(name = "Conductores.findByTipoid", query = "SELECT c FROM Conductores c WHERE c.tipoid = :tipoid")
    , @NamedQuery(name = "Conductores.findByNombre", query = "SELECT c FROM Conductores c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Conductores.findByDireccion", query = "SELECT c FROM Conductores c WHERE c.direccion = :direccion")
    , @NamedQuery(name = "Conductores.findByCiudad", query = "SELECT c FROM Conductores c WHERE c.ciudad = :ciudad")
    , @NamedQuery(name = "Conductores.findByDepartamento", query = "SELECT c FROM Conductores c WHERE c.departamento = :departamento")
    , @NamedQuery(name = "Conductores.findByPais", query = "SELECT c FROM Conductores c WHERE c.pais = :pais")
    , @NamedQuery(name = "Conductores.findByTelefono", query = "SELECT c FROM Conductores c WHERE c.telefono = :telefono")})
public class Conductores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numeroid")
    private Integer numeroid;
    @Size(max = 45)
    @Column(name = "tipoid")
    private String tipoid;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 45)
    @Column(name = "ciudad")
    private String ciudad;
    @Size(max = 45)
    @Column(name = "departamento")
    private String departamento;
    @Size(max = 45)
    @Column(name = "pais")
    private String pais;
    @Size(max = 45)
    @Column(name = "telefono")
    private String telefono;
    @OneToMany(mappedBy = "conductor")
    private List<Afiliacion> afiliacionList;

    public Conductores() {
    }

    public Conductores(Integer numeroid) {
        this.numeroid = numeroid;
    }

    public Integer getNumeroid() {
        return numeroid;
    }

    public void setNumeroid(Integer numeroid) {
        this.numeroid = numeroid;
    }

    public String getTipoid() {
        return tipoid;
    }

    public void setTipoid(String tipoid) {
        this.tipoid = tipoid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @XmlTransient
    public List<Afiliacion> getAfiliacionList() {
        return afiliacionList;
    }

    public void setAfiliacionList(List<Afiliacion> afiliacionList) {
        this.afiliacionList = afiliacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numeroid != null ? numeroid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conductores)) {
            return false;
        }
        Conductores other = (Conductores) object;
        if ((this.numeroid == null && other.numeroid != null) || (this.numeroid != null && !this.numeroid.equals(other.numeroid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.admin.model.Conductores[ numeroid=" + numeroid + " ]";
    }
    
}
