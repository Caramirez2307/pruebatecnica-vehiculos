/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "vehiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculo.findAll", query = "SELECT v FROM Vehiculo v")
    , @NamedQuery(name = "Vehiculo.findByPlaca", query = "SELECT v FROM Vehiculo v WHERE v.placa = :placa")
    , @NamedQuery(name = "Vehiculo.findByMotor", query = "SELECT v FROM Vehiculo v WHERE v.motor = :motor")
    , @NamedQuery(name = "Vehiculo.findByChasis", query = "SELECT v FROM Vehiculo v WHERE v.chasis = :chasis")
    , @NamedQuery(name = "Vehiculo.findByModelo", query = "SELECT v FROM Vehiculo v WHERE v.modelo = :modelo")
    , @NamedQuery(name = "Vehiculo.findByFechamatricula", query = "SELECT v FROM Vehiculo v WHERE v.fechamatricula = :fechamatricula")
    , @NamedQuery(name = "Vehiculo.findByPasajerossentados", query = "SELECT v FROM Vehiculo v WHERE v.pasajerossentados = :pasajerossentados")
    , @NamedQuery(name = "Vehiculo.findByPasajerospie", query = "SELECT v FROM Vehiculo v WHERE v.pasajerospie = :pasajerospie")
    , @NamedQuery(name = "Vehiculo.findByPesoseco", query = "SELECT v FROM Vehiculo v WHERE v.pesoseco = :pesoseco")
    , @NamedQuery(name = "Vehiculo.findByPesobruto", query = "SELECT v FROM Vehiculo v WHERE v.pesobruto = :pesobruto")
    , @NamedQuery(name = "Vehiculo.findByCantidadpuertas", query = "SELECT v FROM Vehiculo v WHERE v.cantidadpuertas = :cantidadpuertas")
    , @NamedQuery(name = "Vehiculo.findByMarca", query = "SELECT v FROM Vehiculo v WHERE v.marca = :marca")
    , @NamedQuery(name = "Vehiculo.findByLinea", query = "SELECT v FROM Vehiculo v WHERE v.linea = :linea")})
public class Vehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "placa")
    private String placa;
    @Size(max = 45)
    @Column(name = "motor")
    private String motor;
    @Size(max = 45)
    @Column(name = "chasis")
    private String chasis;
    @Size(max = 45)
    @Column(name = "modelo")
    private String modelo;
    @Size(max = 45)
    @Column(name = "fechamatricula")
    private String fechamatricula;
    @Column(name = "pasajerossentados")
    private Integer pasajerossentados;
    @Column(name = "pasajerospie")
    private Integer pasajerospie;
    @Column(name = "pesoseco")
    private Integer pesoseco;
    @Column(name = "pesobruto")
    private Integer pesobruto;
    @Column(name = "cantidadpuertas")
    private Integer cantidadpuertas;
    @Size(max = 45)
    @Column(name = "marca")
    private String marca;
    @Size(max = 45)
    @Column(name = "linea")
    private String linea;
    @OneToMany(mappedBy = "vehiculo")
    private List<Afiliacion> afiliacionList;

    public Vehiculo() {
    }

    public Vehiculo(String placa) {
        this.placa = placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFechamatricula() {
        return fechamatricula;
    }

    public void setFechamatricula(String fechamatricula) {
        this.fechamatricula = fechamatricula;
    }

    public Integer getPasajerossentados() {
        return pasajerossentados;
    }

    public void setPasajerossentados(Integer pasajerossentados) {
        this.pasajerossentados = pasajerossentados;
    }

    public Integer getPasajerospie() {
        return pasajerospie;
    }

    public void setPasajerospie(Integer pasajerospie) {
        this.pasajerospie = pasajerospie;
    }

    public Integer getPesoseco() {
        return pesoseco;
    }

    public void setPesoseco(Integer pesoseco) {
        this.pesoseco = pesoseco;
    }

    public Integer getPesobruto() {
        return pesobruto;
    }

    public void setPesobruto(Integer pesobruto) {
        this.pesobruto = pesobruto;
    }

    public Integer getCantidadpuertas() {
        return cantidadpuertas;
    }

    public void setCantidadpuertas(Integer cantidadpuertas) {
        this.cantidadpuertas = cantidadpuertas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    @XmlTransient
    public List<Afiliacion> getAfiliacionList() {
        return afiliacionList;
    }

    public void setAfiliacionList(List<Afiliacion> afiliacionList) {
        this.afiliacionList = afiliacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placa != null ? placa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculo)) {
            return false;
        }
        Vehiculo other = (Vehiculo) object;
        if ((this.placa == null && other.placa != null) || (this.placa != null && !this.placa.equals(other.placa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.admin.model.Vehiculo[ placa=" + placa + " ]";
    }
    
}
